function! screenshot#Capture(filename)
  :echon ''
  :silent exec "!" . g:vim_screenshot_path . "/bin/capture-terminal.sh " . a:filename
endfunction

function! screenshot#SaveBuffersToPDF()
  redraw!
  let l:outpath = "./screenshot_tmp"
  call mkdir(l:outpath)

  let l:buffers = filter(range(1, bufnr('$')), 'bufexists(v:val)')
  for l:buffer in l:buffers
    execute "buffer " . l:buffer
    redraw!
    sleep 100m
    let l:slideImage = l:outpath . "/image" . screenshot#PrePad(l:buffer, 3, 0) . ".png"
    call screenshot#Capture(l:slideImage)
    sleep 100m
  endfor

  :silent exec "!" . g:vim_screenshot_path . "/bin/build-pdf.sh " . l:outpath . " buffers.pdf"

  call delete(l:outpath, "rf")
endfunction

function! screenshot#PrePad(s,amt,...)
    if a:0 > 0
        let char = a:1
    else
        let char = ' '
    endif
    return repeat(char,a:amt - len(a:s)) . a:s
endfunction

