#!/bin/sh
FILEPATH=$1
OUTFILE=$2
for PNG_FILE in "${FILEPATH}/*.png" ; do 
  PDF_FILE=$(echo "${PNG_FILE}" | sed -e 's/png$/pdf/')
  convert "${PNG_FILE}" -resize 2500x3080 -units PixelsPerInch -density 300x300 "${PDF_FILE}"
done
pdftk "${FILEPATH}/*.pdf" cat output "${OUTFILE}"
