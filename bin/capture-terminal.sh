#!/bin/sh

FILENAME=$1

if [ "x$FILENAME" = "x" ] ; then
  >&2 echo "Usage: $0 <filename>"
  exit 1
fi

WID=$(xdotool getmouselocation | sed -E 's/.*window:([0-9]*)/\1/')
import -window $WID -delay 100 "${FILENAME}"
