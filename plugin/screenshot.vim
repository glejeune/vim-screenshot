if exists("g:loaded_vim_screenshot")
    finish
endif
let g:loaded_vim_screenshot = 1

let g:vim_screenshot_path=expand('<sfile>:p:h:h')

command! -nargs=1 VSCapture call screenshot#Capture(<args>)
command! -nargs=0 VSAllBuffersToPDF call screenshot#SaveBuffersToPDF()
